/* 
    build指向静态资源存储库
*/
const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));


app.get('/user', (req, res) => {
    res.json({name:'zhangpeng'})
})

app.listen(3000)