# webpack4搭建web多页网站

#### 介绍
    webpack4搭建多页面web网站
    1、html压缩打包（单行，去除双引号）
    2、js处理
        1）高级语法转低级语法、es6转es5
        2）压缩打包、单行
    3、css处理
        1）添加浏览器前缀
        2）支持@import引入语法
        3）less转css
        4）打包压缩
    4、全局变量的引用
    5、图片处理
        1）css使用背景图片地址处理
        2）js中引用图片处理
        3）html页面中引用图片处理
    6、配置source-map 
        1）增加映射文件 帮我们调试源代码
    7、webpack跨域处理
    8、多页面处理
        1）watch实时压缩打包监听
        2）配置本地服务器，添加静态资源的cdn前缀
    9、resolve属性配置
        1）modules 解析第三方包
        2）extensions 扩展名
        3）alias 路径别名
    10、定义环境变量
    11、区分不同环境
        1）抽离公共webpack.base.js
        2）分离dev环境webpack.dev.js
        3）分离prod环境webpack.prod.js
    12、webpack优化
        1）noParse
        2）include、 exclude
        3）ignorePlugin
        4）dllPlugin
        5）happypack
    13、抽离公共代码
    14、懒加载
    15、热更新
    

    
#### 软件架构
软件架构说明


#### 安装教程

1.  电脑安装node.js（下载node.js http://nodejs.cn/download/）
2.  git clone https://gitee.com/zp_1806/webback4_building_web_site.git

#### 使用说明

1.  根目录文件下 cmd命令 npm install 
2.  根目录文件下 cmd命令 node sever.js 开启本地服务
3.  根目录文件下 重新打开一个cmd窗口 npm run build 
4.  打开浏览器访问 http://localhost:3000
5.  npm run build 生产环境打包
6.  npm run build:dev dev环境打包
7.  npm run dev 
