let path = require('path');
let webpack = require('webpack');
module.exports = {
    mode: 'development',
    entry: {
        jQuery: ['jquery']
    },
    output: {
        filename: '[name].dll.js', // 产生文件名
        path: path.resolve(__dirname, 'build'),
        library: '[name]',
    },
    plugins: [
        new webpack.DllPlugin({
            name: '[name]',
            path: path.resolve(__dirname, 'build', 'manifest.json')
        }),
    ]
}