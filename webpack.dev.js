const { smart } = require('webpack-merge');
const base = require('./webpack.base.js');

module.exports = smart(base, {
    mode: 'development',
    // devtool: 'source-map', // 增加映射文件 帮我们调试源代码
    // devtool: 'eval-source-map',  // 不会产生map文件，可以显示行和列
    // devtool: 'cheap-module-source-map',  // 产生后你可以保留起来
    devtool: 'cheap-module-eval-source-map', // 不会产生文件 集成在打包后的文件中 不会产生列
    devServer: { // 开发服务器的配置
        // open: true,
        port: 9999, // 端口号
        open: true, // 自动打开浏览器
        progress: true, // 进度打印
        contentBase: './build', // 根目录指向
        before(app) { // 只是模拟接口数据
            app.get('/getname',(req, res)=>{
                res.json({name:'peng_zhang'})
            })
        },
        // proxy: { // 代理
        //     '/api': {
        //         target: 'http://localhost:3000',
        //         pathRewrite: {
        //             '/api': ''
        //         }
        //     }
        // },
        hot: true,
    },
    watch: true,
    watchOptions: {
        poll: 1000, // 没秒1000次
        aggregateTimeout: 500, // 防抖
        ignored: /node_modules/, // 不需要监控的文件
    },

})