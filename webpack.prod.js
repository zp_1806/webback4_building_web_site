const { smart } = require('webpack-merge');
const base = require('./webpack.base.js');
// 优化压缩css
const OptimizeCss = require('optimize-css-assets-webpack-plugin');
// 优化压缩js
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = smart(base, {
    mode: 'production',
    optimization: { // 优化项
        minimizer: [
            // 优化压缩js
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // 错误打印
            }),
            new OptimizeCss(), // 优化压缩css
        ],
    },
    
})