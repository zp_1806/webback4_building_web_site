import { getDate } from 'utils';
import $ from 'jquery';
import avatarImage from '@/assets/avatar.jpg';
import moment from 'moment';
import 'moment/locale/zh-cn';
import '@/less/index.less';
let date = moment().format('MMMM Do YYYY, h:mm:ss a');
$('.date').html(date);
$('.img').append(`<img src=${avatarImage} width=200 height=200 />`)
$('body').append(`<div>DEV=${DEV}</div>`)
$('body').append(`<div>FLAG=${FLAG}</div>`)

$('.date').on('click', function() {
    let date = moment().format('MMMM Do YYYY, h:mm:ss a');
    $('.date').html(date)
})
// 点击异步加载数据
$('.async_loading').on('click', function() {
    // es6 草案中提到的 异步加载
    import('../source.js').then(data => {
        console.log('source.js:', data.default)
    })
})