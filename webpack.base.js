
const path = require('path');
// 生成 html
const HtmlWebpackPlugin = require('html-webpack-plugin');
// head写入css link路径
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// 
const webpack = require('webpack');
// 多线程打包
const Happypack = require('happypack');
// 清除clean
const { CleanWebpackPlugin } = require
('clean-webpack-plugin');
// copy 插件
const CopyPlugin = require('copy-webpack-plugin');
// 在html下方插入script标签
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
module.exports = {
    optimization: {
        splitChunks: { // 分割代码块
            cacheGroups: { // 缓存组
                common: { // 公共的模块
                    chunks: 'initial',
                    minSize: 0,
                    minChunks: 2,
                },
                vendor: {
                    priority: 1, // 权重
                    test: /node_modules/, // 若是node_modules把你抽离出来
                    chunks: 'initial',
                    minSize: 0,
                    minChunks: 2,
                }
            }
        }
    },
    entry: { //入口
        home: path.resolve(__dirname, './src/js/index.js'),
        detail: path.resolve(__dirname, './src/js/detail.js'),
    }, 
    output: {
        filename: 'js/[name].[hash:8].js', // 打包后的文件名
        path: path.resolve(__dirname, 'build'), // 打包后的路径，必须是一个绝对路径
        // 打包路径 加cdn前缀
        // publicPath: 'http://localhost:3000',
    },
    plugins: [ //  数组 放着所有的webpack插件
        new webpack.DefinePlugin({ // 定义环境变量
            DEV: JSON.stringify('dev'), // 字符串需要用JSON.stringify
            FLAG: true,
        }),
        new Happypack({ // 多线程打包js
            id: 'js',
            use: [{
                loader: 'babel-loader',
                options: { // 用babel-loader 把es6 转es5
                    presets: [
                        '@babel/preset-env',
                    ],
                    plugins: [
                        [
                            '@babel/plugin-proposal-decorators',
                            {
                                "legacy": true
                            }
                        ],
                        [
                            '@babel/plugin-proposal-class-properties',
                            {
                                "loose": true
                            }
                        ],
                        // 处理更高 es7 语法转换低级语法
                        '@babel/plugin-transform-runtime',
                        '@babel/plugin-syntax-dynamic-import',
                    ]
                }
            },]
        }),
        new Happypack({
            id: 'css',
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'postcss-loader',
            ],
        }),
        new webpack.IgnorePlugin(/\.\local/,/moment/), // 忽略moment语言包打包
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
            minify: { // 压缩 HTML
                removeComments: true, // 去除注释
                removeAttributeQuotes: true, // 去除双引号
                collapseWhitespace: true, // 单行
            },
            hash: true, // hash 解决浏览器文件缓存
            chunks: ['home']
        }),
        new HtmlWebpackPlugin({
            template: './src/detail.html',
            filename: 'detail.html',
            minify: { // 压缩 HTML
                removeComments: true, // 去除注释
                removeAttributeQuotes: true, // 去除双引号
                collapseWhitespace: true, // 单行
            },
            hash: true, // hash 解决浏览器文件缓存
            chunks: ['detail']
        }),
        // new AddAssetHtmlPlugin({
        //     filepath: path.join(__dirname, './node_modules/jquery/dist/jquery.min.js'),
        //     hash: true,
        //     typeOfAsset: 'js',
        //     includeSourcemap: false,
        //     outputPath: '../build/lib/',
        //     publicPath: '/lib',
        // }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[hash:8].css',
            // 打包路径 加cdn前缀
            // publicPath: '/'
        }),
        // 提供插件 在每个模块中都注入$
        new webpack.ProvidePlugin({
            // $: 'jquery/dist/jquery.min.js',
        }),

        // copy文件
        new CopyPlugin([
            { from: 'docs', to: 'docs' },
        ]),
        // 在文件中添加 版权注释
        new webpack.BannerPlugin('made in zhangpeng!'),
        // 清空目录
        new CleanWebpackPlugin(),
        // 热更新
        new webpack.NamedModulesPlugin(), // 打印更新的模块路径
        new webpack.HotModuleReplacementPlugin(), // 热更新插件
    ],
    externals: {
        // 模块是外部引入的 不需要打包
        // jquery: '$',
    },
    module: { // 模块
        noParse: /jquery/,
        rules: [  // 规则
            // css-loader 支持 @import 语法
            // style-loader 他是把css插入head标签中
            // loader的特点 希望单一
            // loader的用法 字符串只用一个loader
            // 多个loader需要 []
            // loader的顺序 默认是从右向左执行
            // loader还可以写成对象
            {
                // 处理html中使用相对路劲引入的图片
                // url-loader 1.1.2 file-loader 3.0.1
                test: /\.(htm|html)$/i,
                use: ['html-withimg-loader'],
            },

            {
                test: /\.tpl$/,
                use: 'ejs-loader',
            },

            {
                test: /\.(png|jpg|gif|jpeg|ico)$/i,
                // 做一个限制， 当图片小于200K时，用base64转化，不用http请求
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 244*1024,
                            // 输出路径
                            outputPath: 'img/',
                            // 配置图片cdn 域名
                            publicPath: '/img'
                        }
                    },
                    // {
                    //     loader: 'image-webpack-loader',
                    // }
                ]
            },
            // $暴露给window，供window.$ 调用
            // {
            //     test: require.resolve('jquery'),
            //     use: 'expose-loader?$',
            // },
            // eslint语法检测
            // {
            //     test: /\.js$/,
            //     use: {
            //         loader: 'eslint-loader',
            //         options: {
            //             enforce: 'pre', // previous 强制提前执行 post 推迟执行
            //         }
            //     },
            // },
            {
                test: /\.js$/,
                use: 'happypack/loader?id=js',
                include: path.resolve(__dirname, 'src'),
                exclude: '/node_modules/'
            },
            {
                test: /\.css$/,
                use: 'happypack/loader?id=css',
            },
            {
                // 处理less文件 sass stylus node-sass sass-loader
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader', // @import 解析路径
                    'postcss-loader',
                    'less-loader', // less -> css
                ],
            },
        ]
    },
    resolve: { // 解析第三方包 common
        modules: [path.resolve('node_modules')],
        extensions: ['.js', '.css', '.less', '.scss', '.json'], // 省略扩展名
        // mainFields: ['style', 'main'],
        // mainFiles: [], // 入口文件的名字
        alias: { // 别名
            '@': path.join(__dirname, './src'),
            utils: path.join(__dirname, './src/utils/utils.js'),
        }
    }
}